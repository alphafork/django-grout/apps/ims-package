from django.apps import AppConfig


class IMSPackageConfig(AppConfig):
    default_auto_field = "django.db.models.BigAutoField"
    name = "ims_package"
