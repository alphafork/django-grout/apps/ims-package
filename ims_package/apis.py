from ims_api_permission.apis import APIMethodPermission
from ims_base.apis import BaseAPIViewSet
from ims_student_registration.apis import (
    BatchStudent,
    CourseStudentAPI,
    GenericStudentDetailsAPIView,
    StudentBatchFilter,
    StudentRegistrationAPI,
    StudentRegistrationFilter,
    StudentSearchFilter,
)
from ims_student_registration.models import CourseStudent
from ims_user.apis import UserFilter
from rest_framework.exceptions import NotFound
from rest_framework.filters import BaseFilterBackend
from reusable_models import get_model_from_string

from ims_package.models import Package, PackageStudent
from ims_package.serializers import (
    PackageBatchMigrationSerializer,
    PackageBatchStudentSerializer,
    PackageCourseStudentSerializer,
    PackageStudentBatchMigrationSerializer,
    PackageStudentRegistrationSerializer,
    PackageStudentSerialzer,
    StudentApprovalSerializer,
)

Batch = get_model_from_string("BATCH")
StudentRegistration = get_model_from_string("STUDENT_REGISTRATION")
StudentFee = get_model_from_string("STUDENT_FEE")


class StudentPackageFilter(BaseFilterBackend):
    def filter_queryset(self, request, queryset, view):
        filter_map = {}
        if "package_id" in request.query_params:
            package_student_registrations = PackageStudent.objects.filter(
                package=request.query_params["package_id"]
            ).values("registration_id")
            filter_map = {"id__in": package_student_registrations}
        return queryset.filter(**filter_map)


class StudentEnrollmentFilter(BaseFilterBackend):
    def filter_queryset(self, request, queryset, view):
        filter_map = {}
        if "has_enrolled_any" in request.query_params:
            package_registrations = PackageStudent.objects.exclude(
                registration__status="ap"
            ).values_list("registration", flat=True)
            course_registrations = CourseStudent.objects.exclude(
                registration__status="ap"
            ).values_list("registration", flat=True)
            registrations = list(course_registrations) + list(package_registrations)
            batch_registrations = BatchStudent.objects.filter(
                registration__in=registrations
            ).values_list("registration", flat=True)
            if request.query_params["has_enrolled_any"] == "True":
                filter_map = {"id__in": batch_registrations}
            elif request.query_params["has_enrolled_any"] == "False":
                non_batch_registrations = []
                for registration in registrations:
                    if registration not in batch_registrations:
                        non_batch_registrations.append(registration)
                filter_map = {"id__in": non_batch_registrations}
        return queryset.filter(**filter_map)


class PackageBatchFilter(BaseFilterBackend):
    def filter_queryset(self, request, queryset, view):
        if "batch_id" in request.query_params:
            course = Batch.objects.get(id=request.query_params["batch_id"]).course
            return queryset.filter(**{"courses": course})
        return queryset


class BatchPackageFilter(BaseFilterBackend):
    def filter_queryset(self, request, queryset, view):
        if "package_id" in request.query_params:
            courses = Package.objects.get(
                id=request.query_params["package_id"]
            ).courses.all()
            if "registration_id" in request.query_params:
                registration = StudentRegistration.objects.get(
                    pk=request.query_params["registration_id"]
                )
                enrolled_courses = registration.batchstudent_set.filter(
                    batch__course__in=courses
                ).values_list("batch__course_id", flat=True)
                courses = courses.exclude(id__in=enrolled_courses)
            return queryset.filter(**{"course__in": courses})
        return queryset


class PackageStudentAPI(CourseStudentAPI):
    model_class = PackageStudent
    serializer_class = PackageStudentSerialzer


class PackageCourseStudentAPI(CourseStudentAPI):
    model_class = CourseStudent
    serializer_class = PackageCourseStudentSerializer


class PackageBatchStudentAPI(BaseAPIViewSet):
    model_class = BatchStudent
    serializer_class = PackageBatchStudentSerializer
    permission_classes = BaseAPIViewSet.permission_classes + [APIMethodPermission]
    managed_permissions = {
        "OPTIONS": ["Admin", "Manager", "Faculty", "Student"],
        "GET": ["Admin", "Manager", "Faculty", "Student"],
        "POST": ["Admin", "Manager"],
        "PUT": ["Admin"],
        "PATCH": ["Admin"],
        "DELETE": ["Admin"],
    }

    def get_serializer_context(self):
        serializer_context = super().get_serializer_context()
        serializer_context["package_id"] = self.request.query_params.get(
            "package_id", None
        )
        serializer_context["registration_id"] = self.request.query_params.get(
            "registration_id", None
        )
        return serializer_context

    def filter_queryset(self, queryset):
        queryset = super().filter_queryset(queryset)
        package_id = self.request.query_params.get("package_id", None)
        if package_id and Package.objects.filter(id=package_id).exists():
            package_student = PackageStudent.objects.filter(package_id=package_id)
            if package_student:
                return queryset.filter(registration=package_student[0].registration)
            else:
                raise NotFound(code=404, detail="No registrations found.")
        raise NotFound(code=404, detail="Package does not exist.")


class PackageBatchAPI(BaseAPIViewSet):
    model_class = Batch
    filter_backends = [BatchPackageFilter]

    def get_queryset(self):
        return super().get_queryset().filter(is_registration_open=True)


class BatchPackageAPI(BaseAPIViewSet):
    model_class = Package
    filter_backends = [PackageBatchFilter]


class StudentPackageListAPI(GenericStudentDetailsAPIView):
    def get_response_data(self, student_id):
        packages = Package.objects.values()
        data = []
        for package in packages:
            package_student = PackageStudent.objects.filter(
                registration__student_id=student_id, package_id=package["id"]
            )
            package["status"] = "unregistered"
            if package_student.exists():
                registration = package_student[0].registration
                package["registration_id"] = registration.id
                package["status"] = dict(registration.REGISTRATION_STATUS)[
                    registration.status
                ]
            data.append(package)
        return data


class PackageStudentRegistrationAPI(StudentRegistrationAPI):
    filter_backends = BaseAPIViewSet.filter_backends + [
        UserFilter,
        StudentSearchFilter,
        StudentRegistrationFilter,
        StudentPackageFilter,
        StudentBatchFilter,
        StudentEnrollmentFilter,
    ]
    model_class = StudentRegistration
    serializer_class = PackageStudentRegistrationSerializer


class PackageStudentBatchMigrationAPI(BaseAPIViewSet):
    model_class = BatchStudent
    serializer_class = PackageStudentBatchMigrationSerializer
    permission_classes = BaseAPIViewSet.permission_classes + [APIMethodPermission]
    managed_permissions = {
        "OPTIONS": ["Admin", "Manager"],
        "GET": ["Admin", "Manager"],
        "POST": ["Admin", "Manager"],
        "PUT": ["Admin", "Manager"],
        "PATCH": ["Admin", "Manager"],
    }


class PackageBatchMigrationAPI(BaseAPIViewSet):
    model_class = BatchStudent
    serializer_class = PackageBatchMigrationSerializer
    permission_classes = BaseAPIViewSet.permission_classes + [APIMethodPermission]
    managed_permissions = {
        "OPTIONS": ["Admin", "Manager"],
        "GET": ["Admin", "Manager"],
        "POST": ["Admin", "Manager"],
        "PUT": ["Admin", "Manager"],
        "PATCH": ["Admin", "Manager"],
    }


class StudentApprovalAPI(BaseAPIViewSet):
    model_class = StudentFee
    serializer_class = StudentApprovalSerializer
