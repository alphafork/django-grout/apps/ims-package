from django.db import models
from django.db.models.signals import post_save
from django.dispatch import receiver
from ims_base.models import AbstractBaseDetail, AbstractLog
from reusable_models import get_model_from_string

StudentRegistration = get_model_from_string("STUDENT_REGISTRATION")
BatchStudent = get_model_from_string("BATCH_STUDENT")
Course = get_model_from_string("COURSE")


class Package(AbstractBaseDetail):
    package_code = models.CharField(max_length=255, null=True, blank=True)
    is_registration_open = models.BooleanField(default=True)
    duration_days = models.PositiveSmallIntegerField(null=True, blank=True)
    introduced_date = models.DateField(null=True, blank=True)
    courses = models.ManyToManyField(Course, blank=True)
    fee = models.PositiveIntegerField(null=True, blank=True)

    def __str__(self):
        return self.name


class PackageStudent(AbstractLog):
    registration = models.OneToOneField(
        StudentRegistration,
        on_delete=models.CASCADE,
        primary_key=True,
    )
    package = models.ForeignKey(Package, on_delete=models.CASCADE)

    def __str__(self):
        return f"{self.registration.__str__()} | {self.package.__str__()}"


@receiver(post_save, sender=BatchStudent)
def update_studentregistration_status_on_package_completion(instance, **kwargs):
    if instance.status == "co":
        package_student = getattr(instance.registration, "packagestudent", None)
        if package_student:
            courses = package_student.package.courses.all()
            program_completed = True
            for course in courses:
                if not BatchStudent.objects.filter(
                    registration=instance.registration,
                    status="co",
                    batch__course=course,
                ).exists():
                    program_completed = False
            if program_completed:
                instance.registration.status = "co"
                instance.registration.save()
