from datetime import date

from django.db.models import Value
from django.db.models.functions import Concat
from ims_attendance.services import get_course_attendance
from ims_exam.services import get_course_exam_result
from ims_workday.services import add_workdays_to_date
from reusable_models import get_model_from_string

from ims_package.models import Package

Batch = get_model_from_string("BATCH")
BatchStudent = get_model_from_string("BATCH_STUDENT")
DailySchedule = get_model_from_string("DAILY_SCHEDULE")


def get_package_batch_choices(package_id):
    courses = []
    if package_id and Package.objects.filter(id=package_id).exists():
        courses = Package.objects.get(pk=package_id).courses.all()
    batches = Batch.objects.filter(course__in=courses)
    return batches


def get_package_course_batch_data(registration, course):
    batch_student_list = BatchStudent.objects.filter(
        registration=registration, batch__course=course
    )
    batches = []
    for batch_student in batch_student_list:
        batches.append(
            {
                "batch_student": batch_student.id,
                "batch": batch_student.batch.id,
                "batch_name": batch_student.batch.name,
                "status": dict(batch_student.BATCH_STUDENT_STATUS)[
                    batch_student.status
                ],
                "enrolled_date": batch_student.enrolled_date,
            }
        )
    return {"batches": batches}


def get_package_course_active_batch_data(registration, course):
    batch_student = BatchStudent.objects.filter(
        registration=registration,
        batch__course=course,
    ).exclude(status="mi")
    batch = None
    if batch_student.exists():
        batch_student = batch_student.first()
        batch = {
            "batch_student": batch_student.id,
            "batch": batch_student.batch.id,
            "batch_name": batch_student.batch.name,
            "status": dict(batch_student.BATCH_STUDENT_STATUS)[batch_student.status],
            "enrolled_date": batch_student.enrolled_date,
        }
        batch["todays_schedule"] = (
            DailySchedule.objects.filter(
                date=date.today(),
                schedule__batch=batch_student.batch,
            )
            .annotate(
                faculty_name=Concat(
                    "faculty__staff__user__first_name",
                    Value(" "),
                    "faculty__staff__user__last_name",
                )
            )
            .values("slot", "faculty_name")
            or None
        )
    return {"batch": batch}


def get_package_course_data(registration):
    courses = registration.packagestudent.package.courses.all()
    course_list = []
    for course in courses:
        course_list.append(
            {
                "course": course.id,
                "course_name": course.name,
                **get_package_course_active_batch_data(registration, course),
                **get_course_exam_result(registration, course),
                **get_course_attendance(registration, course),
            }
        )
    return {"courses": course_list}


def get_package_data(registration):
    packagestudent = getattr(registration, "packagestudent", None)
    if packagestudent:
        batch_student_list = BatchStudent.objects.filter(registration=registration)
        if batch_student_list:
            start_date = (
                batch_student_list.order_by("enrolled_date").first().enrolled_date
            )
            if packagestudent.package.duration_days:
                end_date = add_workdays_to_date(
                    start_date, packagestudent.package.duration_days
                )
            else:
                end_date = None
        else:
            start_date, end_date = None, None
        package = {
            "id": packagestudent.package.id,
            "name": packagestudent.package.name,
            "start_date": start_date,
            "end_date": end_date,
            **get_package_course_data(registration),
            **get_enrollment_status(registration),
        }
        return {"package": package}
    return {"package": None}


def get_enrollment_status(registration):
    courses = registration.packagestudent.package.courses.all()
    courses_count = courses.count()
    enrolled_batches_count = 0
    for course in courses:
        if BatchStudent.objects.filter(
            registration=registration, batch__course=course, status__in=["ac", "co"]
        ).exists():
            enrolled_batches_count += 1
    if enrolled_batches_count == 0:
        return {"enrollment_status": "none", "enrollment_count": f"0/{courses_count}"}
    elif enrolled_batches_count < courses_count:
        return {
            "enrollment_status": "partial",
            "enrollment_count": f"{enrolled_batches_count}/{courses_count}",
        }
    else:
        return {
            "enrollment_status": "all",
            "enrollment_count": f"{courses_count}/{courses_count}",
        }
