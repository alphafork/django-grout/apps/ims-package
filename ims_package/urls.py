from django.urls import include, path
from rest_framework import routers
from rest_framework.urlpatterns import format_suffix_patterns

from .apis import (
    BatchPackageAPI,
    PackageBatchAPI,
    PackageBatchMigrationAPI,
    PackageBatchStudentAPI,
    PackageCourseStudentAPI,
    PackageStudentAPI,
    PackageStudentBatchMigrationAPI,
    PackageStudentRegistrationAPI,
    StudentApprovalAPI,
    StudentPackageListAPI,
)

router = routers.SimpleRouter()
router.register(r"student-application", PackageStudentAPI, "package-apply")
router.register(r"course-application", PackageCourseStudentAPI, "course-apply")
router.register(r"student-enrollment", PackageBatchStudentAPI, "package-enroll")
router.register(
    r"student-batch-migration",
    PackageStudentBatchMigrationAPI,
    "student-batch-migration",
)
router.register(
    r"batch-migration",
    PackageBatchMigrationAPI,
    "batch-migration",
)
router.register(
    r"student-registration",
    PackageStudentRegistrationAPI,
    "student-registration",
)
router.register(
    r"student-approval",
    StudentApprovalAPI,
    "student-approval",
)
router.register(
    r"student-details",
    PackageStudentRegistrationAPI,
    "student-details",
)
router.register(r"batch", PackageBatchAPI, "package-batch")
router.register(r"batch-package", BatchPackageAPI, "batch-package")

urlpatterns = [
    path("packages/<int:pk>/", StudentPackageListAPI.as_view()),
    path("", include(router.urls)),
]

urlpatterns = format_suffix_patterns(urlpatterns, allowed=["json"])
