from datetime import datetime, timedelta

from ims_attendance.services import get_course_attendance
from ims_base.serializers import BaseModelSerializer
from ims_exam.services import get_course_exam_result
from ims_student_fee.serializers import StudentFeeSerializer
from ims_student_registration.serializers import (
    BatchMigrationSerializer,
    BatchStudentSerializer,
    CourseStudentSerializer,
    StudentBatchMigrationSerializer,
    StudentRegistrationSerializer,
)
from ims_student_registration.services import registration_no_generator
from ims_workday.services import get_adjacent_workday
from rest_framework import serializers
from reusable_models import get_model_from_string

from ims_package.models import PackageStudent

from .services import get_package_batch_choices, get_package_data

Student = get_model_from_string("STUDENT")
StudentRegistration = get_model_from_string("STUDENT_REGISTRATION")
Course = get_model_from_string("COURSE")
CourseStudent = get_model_from_string("COURSE_STUDENT")
Batch = get_model_from_string("BATCH")
BatchStudent = get_model_from_string("BATCH_STUDENT")
Certificate = get_model_from_string("CERTIFICATE")
Internship = get_model_from_string("INTERNSHIP")


class PackageStudentSerialzer(BaseModelSerializer):
    student = serializers.PrimaryKeyRelatedField(
        write_only=True, queryset=Student.objects.all()
    )

    class Meta(BaseModelSerializer.Meta):
        excluded_fields = BaseModelSerializer.Meta.excluded_fields + ["registration"]
        extra_meta = {
            "student": {
                "related_model_url": "/student/student",
            },
        }

    def validate(self, attrs):
        validated_data = super().validate(attrs)
        student = validated_data["student"]
        package = validated_data["package"]
        user = self.context["request"].user
        if (
            not user.groups.filter(
                name__in=["Admin", "Manager", "Accountant"]
            ).exclude()
            and student.user != user
        ):
            raise serializers.ValidationError({"student": "Student value not allowed."})
        student_registrations = StudentRegistration.objects.filter(
            student_id=student, status__in=["ap", "pr"]
        )
        if (
            PackageStudent.objects.filter(registration__in=student_registrations)
            .filter(package=package)
            .exists()
        ):
            raise serializers.ValidationError(
                {"package": ("You have already applied for this package.")}
            )
        if (
            CourseStudent.objects.filter(registration__in=student_registrations)
            .filter(course__in=package.courses.all())
            .exists()
        ):
            raise serializers.ValidationError(
                {"package": ("You have already applied for a course in this package.")}
            )

        return validated_data

    def create(self, validated_data):
        student_registration = StudentRegistration(
            registration_no=registration_no_generator(),
            student=validated_data.pop("student"),
            applied_date=datetime.now().strftime("%Y-%m-%d"),
            status="ap",
        )
        student_registration.save()
        validated_data["registration"] = student_registration
        return super().create(validated_data)

    def to_representation(self, instance):
        data = super().to_representation(instance)
        student = {
            "id": instance.registration.student.user.id,
            "name": instance.registration.student.user.get_full_name(),
            "email": instance.registration.student.user.email,
        }
        data["student"] = student
        registration_obj = {
            "id": instance.registration.id,
            "number": instance.registration.registration_no,
            "status": dict(instance.registration.REGISTRATION_STATUS)[
                instance.registration.status
            ],
        }
        data["registration"] = registration_obj
        return data


class PackageCourseStudentSerializer(CourseStudentSerializer):
    def validate(self, attrs):
        validated_data = super().validate(attrs)

        if PackageStudent.objects.filter(
            package__courses=validated_data["course"],
            registration__student=validated_data["student"],
        ).exists():
            raise serializers.ValidationError(
                {"course": "You have already applied for this course."}
            )

        return validated_data


class PackageBatchStudentSerializer(BatchStudentSerializer):
    def validate(self, attrs):
        validated_data = super().validate(attrs)
        batch = validated_data.get("batch")
        registration = validated_data.get("registration")
        package_student = getattr(registration, "packagestudent", None)
        if package_student:
            package = package_student.package
            batches = get_package_batch_choices(package.id)
            if batch in batches:
                return validated_data
            raise serializers.ValidationError(
                {"batch": "Batch is not found in registered package."}
            )
        raise serializers.ValidationError(
            {"registration": "No packages found with given registration."}
        )

    def get_extra_meta(self):
        extra_meta = super().get_extra_meta() or {}
        package_id = self.context["package_id"]
        batch_list_url = "/package/batch/?"
        if package_id:
            batch_list_url += f"package_id={package_id}"
            registration_id = self.context["registration_id"]
            if registration_id:
                batch_list_url += f"&registration_id={registration_id}"
        extra_meta["batch"] = {
            "related_model_url": batch_list_url,
        }
        return extra_meta


class PackageStudentBatchMigrationSerializer(StudentBatchMigrationSerializer):
    def validate(self, attrs):
        validated_data = super().validate(attrs)
        registration = validated_data["batch_student"].registration
        student = registration.student.user.get_full_name()
        course = validated_data["new_batch"].course
        packagestudent = getattr(registration, "packagestudent", None)
        if packagestudent and course in packagestudent.package.courses.all():
            serializers.ValidationError(
                {
                    "batch_student": (
                        f"{student} | {registration}"
                        + f" is not applied for a course in {packagestudent.package}"
                    )
                }
            )
        return validated_data


class PackageBatchMigrationSerializer(BatchMigrationSerializer):
    batch_student_migration = serializers.ListField(
        write_only=True,
        child=PackageStudentBatchMigrationSerializer(),
    )


class PackageStudentRegistrationSerializer(StudentRegistrationSerializer):
    def to_representation(self, instance):
        data = super().to_representation(instance)
        course = data.pop("course", None)
        if course:
            course["type"] = "course"
            data.update(
                {
                    "program": {
                        **course,
                        "type": "course",
                        **get_course_exam_result(data["id"], course["id"]),
                        **get_course_attendance(data["id"], course["id"]),
                    },
                }
            )
        else:
            package = get_package_data(instance)["package"]
            package["type"] = "package"
            data.update(
                {
                    "program": package,
                }
            )
        return data


class StudentApprovalSerializer(StudentFeeSerializer):
    internship_start_date = serializers.DateField(write_only=True, required=False)

    class Meta(StudentFeeSerializer.Meta):
        pass

    def create(self, validated_data):
        internship_start_date = validated_data.pop("internship_start_date", None)
        student_fee = super().create(validated_data)

        registration = student_fee.registration

        certificate_start_date = get_adjacent_workday(
            "succeeding", registration.applied_date
        )
        certificate_end_date = get_adjacent_workday(
            "preceding", internship_start_date - timedelta(1)
        )
        internship_start_date = get_adjacent_workday(
            "succeeding", internship_start_date
        )
        internship_end_date = get_adjacent_workday("succeeding", registration.exit_date)
        duration = f"{(internship_end_date - internship_start_date).days} days"

        course_student = getattr(registration, "coursestudent", None)
        package_student = getattr(registration, "packagestudent", None)

        program = None
        if course_student:
            program = course_student.course.name
        if package_student:
            program = package_student.package.name

        certificate_data = {
            "registration": registration,
            "start_date": certificate_start_date,
            "end_date": certificate_end_date,
            "date_of_issue": certificate_end_date,
            "is_downloadable": False,
            "program": program,
        }
        internship_data = {
            "registration": registration,
            "start_date": internship_start_date,
            "end_date": internship_end_date,
            "date_of_issue": certificate_end_date,
            "internship_duration": duration,
            "is_downloadable": False,
            "program": program,
            "internship_title": program,
        }
        Internship.objects.create(**internship_data)
        Certificate.objects.create(**certificate_data)
        return student_fee
